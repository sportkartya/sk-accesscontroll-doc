# SK elfogadó API dokumentáció
Az alábbi dokumentum a SPORTKÁRTYA által üzemeltetett beléptető rendszerrel való szinkronizációt lépéseit írja le példákon keresztül.
## Tartalomjegyzék
### [Csatlakozás](http://sk-accesscontroll-doc.readthedocs.io/hu/latest/csatlakozas/)
### [Használat minta](http://sk-accesscontroll-doc.readthedocs.io/hu/latest/hasznalat-minta/)
### [Használat](http://sk-accesscontroll-doc.readthedocs.io/hu/latest/hasznalat/)
---
Jelenlegi verzió: 1.0