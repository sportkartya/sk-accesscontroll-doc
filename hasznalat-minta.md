# Használat minta

Lehetőség van az elfogadó beléptető rendszerében az ügyfél profilját összekapcsolni az általunk visszaadott adatokkal. Ezáltal a beléptetés egyszerűbbé és gyorsabbá válik. Itt található egy minta összekötési megoldás

**Peremfeltétel:** A vendég a recepcióhoz már úgy érkezik, hogy a mobil applikáción keresztül belépett.

## Beléptető ügyfél profil megnyitása
Az elfogadó megnyitja a beléptető rendszerében az vendég profilját. Itt rákattint a **SK ellenőrzés** gombra. 

Az alábbi két eset lehetséges:

### Még nem összepárosított vendég
Ebben az esetben a beléptető rendszerben még nincs eltárolva a vendég SK rendszeréhez tartozó azonosító (subscriber_hash) paramétere.

Javasolt ebben az esetben az [entries](http://sk-accesscontroll-doc.readthedocs.io/hu/latest/hasznalat/#get-acv1entries) végpontot meghívni. Ilyenkor a rendszerünk visszaadja az utolsó 3 órában történt belépéseket. 

![alt text](images/belepesek_lista.png "Belépés lista - minta")

Ez a recepciós számára megjelenik, ahol a belépés alapján kiválaszthatja a vendéget. Amint a megfelelő sorra rákattint, a beléptető rendszer elmenti a subscriber_hash paramétert a vendég profiladatai mellé. Így következő belépésnél már az összepárosított vendég eset fog előfordulni. 

### Már összepárosított vendég

Amennyiben az beléptető adatbázisában van a _subscriber\_hash_ paraméterre egyezés akkor a [/entries?subscriber_hash="{subscriber_hash}"](http://sk-accesscontroll-doc.readthedocs.io/hu/latest/hasznalat/#get-acv1entries]) lekérdezés alapján a rendszerünk visszaadja a vendég elmúlt 3 órában történt belépését. Amennyiben a vendég még nem lépett be erre a hívásra üres válasz érkezik.

Ha érkezik válasz, akkor a belépés sikeres. És a beléptető rendszer további automatizmusai jóváírják a belépést a rendszerben.

