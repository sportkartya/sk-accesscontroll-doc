# Csatlakozás menete

## Beléptető rendszerbe építés

### Fejlesztői token igénylése

A fejlesztéshez biztosítunk egy fejlesztői kulcsot a teszt rendszerünkhöz, amivel a beépítés során tudnak dolgozni, tesztelni.

### Végpontok leírása

A végpontok leírását itt részletesen végigolvashatják: 
[http://sk-accesscontroll-doc.readthedocs.io/hu/latest/hasznalat](http://sk-accesscontroll-doc.readthedocs.io/hu/latest/hasznalat)

A [Csatlakozás minta](http://sk-accesscontroll-doc.readthedocs.io/hu/latest/hasznalat-minta/) menüben található az ajánlott rendszer összekötés és használat menete.

### Élesítés

Amennyiben a fejlesztés készen van, kérjük jelezzék egy rövid tesztelés után, ha minden megfelel, akkor megadjuk az éles hozzáféréseket.

### Fontos információ
Minden egyes elfogadó helyünknek egyedi kulcsot generálunk, ugyanis minden kulcsnak csak a hozzá társított elfogadó belépéseihez van jogosultsága.

### Probéláma esetén

Bármilyen kérdés, egyedi végpont kérés adódik emailben segítséget nyújtunk az **admin[a]sportkartya.hu** email címen.

