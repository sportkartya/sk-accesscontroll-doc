# Általános Információ

**Válasz formátuma**: JSON

**Teszt rendszer URL**: https://dev.sportkartya.hu/api/acv1/

*Minden mintakódot cUrl paranccsal mutatunk be.*

*A tesztrendszerben található személyes adatok el vannak maszkolva!*

# Kötelező elemek minden hívásban

A kapott Access Tokent kötelező a header-ben használni, az alábbi formátumban.

```bash
$curl -i https://dev.sportkartya.hu/api/acv1/{command} -H "Authorization: Bearer {ACCESS_TOKEN}"
```

## GET /acv1/entries

### Leírás
Az utolsó három óra belépéseit adja vissza egy listában. A belépés részletes adatait a GET /acv1/entries/{entry_hash} hívás adja meg. 
### Paraméterek

access_token: {ACCESS_TOKEN} **kötelező**,

lang: hu / en **opcionális, default hu**

### Minta kérés

```bash
$curl -i https://dev.sportkartya.hu/api/acv1/acv1/entries -H "Authorization: Bearer {ACCESS_TOKEN}"
```

### Minta válasz

```json
[   
  {
    "id" : "PLboAq1",
    "status" : "Belépés engedélyezve",
    "status_slug" : "access_granted",
    "used_at" : "2016-12-31T14:06:10.000+01:00",
    "sector_name" : "Oxygen Wellness Központ- Fáy - Fitness",
    "subscriber_hash" : "x3jZA13",
    "subscriber_serial" : "10606",
    "subscriber_first_name" : "Csaba",
    "subscriber_last_name" : "Pintér",
    "subscriber_name" : "Pintér Csaba",
    "photo_url" : "",
    "created_at" : "2017-03-08T10:37:15.000+01:00",
    "updated_at" : "2017-03-08T10:37:15.000+01:00"
  },
  {     
    "id" : "FPsAq1",
    "serial" : "10216",
    "used_at" : "2016-12-31T14:06:10.000+01:00",
    "entry_hash" : "PLboAq1",
    "status" : "Belépés engedélyezve",
    "status_slug" : "access_granted",
    "sector_name" : "Oxygen Wellness Központ- Fáy - Fitness",
    "note" : "access_granted",
    "subscriber_hash" : "DdsCA13",
    "subscriber_first_name" : "István",
    "subscriber_last_name" : "Teszt",
    "subscriber_name" : "Teszt István",
    "photo_url" : null,
    "created_at" : "2017-03-08T10:37:15.000+01:00",
    "updated_at" : "2017-03-08T10:37:15.000+01:00"
  }
]
```

### Magyarázat
##### entry_hash
belépés egyedi azonosítója
##### used_at 
belépés ideje
##### serial
Kártya azonosítója
##### subscriber_name
A felhasználó neve
##### sector_name
A helyszín, ahova belépett a felhasználó (nagyobb helyeken 
több helyszínre is be lehet lépni. pl. V8 uszoda / fitness).
##### photo_url 
A felhasználó fényképe ezt ellenőrzés miatt mindenképp kötelező 
megjeleníteni a felületen
#####subscriber_first_name
Felhasználó keresztneve
##### subscriber_last_name
Felhasználó vezetékneve
##### subscriber_hash
Felhasználó egyedi azonosítója (rendszer tag külső kulcsa)

<!-- ## GET /acv1/entries/{entry_hash}

### Leírás
A beléptetés részletesebb adatait tartalmazza ez a hívás. Abban az esetben, ha a beléptető rendszer össze szeretné kötni a rendszerében található felhasználókkal, ezzel gyorsítva a beléptetésüket, akkor ez a **subscriber_hash** paraméterrel valósítható meg, mint a program ügyfelének idegen kulcsa.

**Az ügyfél serial (kártya azonosítója) nem alkalmas erre a feladatra, mert kártya elvesztés esetén az ügyfél új kártyát kap.**

### Minta kérés

```bash
$curl curl -X GET -H "Authorization: Bearer {ACCESS_TOKEN}" 'https://dev.sportkartya.hu/api/acv1/entries/39vdJAY?lang=hu'
```

### Minta válasz

```json
{
  "entry_hash": "39vdJAY",
  "used_at": "2017-08-31T08:03:21.254+02:00",
  "serial": "00008",
  "subscriber_name": "Sportkártya Admin",
  "sector_name": "Elfogadóhely neve - szolgáltatás",
  "photo_url": "https://etherealwellness.files.wordpress.com/2011/08/crazy_harry1.jpg",
  "subscriber_first_name": "Admin",
  "subscriber_last_name": "Sportkártya",
  "subscriber_hash": "3j1rK23"
}
```

### Magyarázat
##### entry_hash
belépés egyedi azonosítója
##### used_at 
belépés ideje
##### serial
Kártya azonosítója
##### subscriber_name
A felhasználó neve
##### sector_name
A helyszín, ahova belépett a felhasználó (nagyobb helyeken 
több helyszínre is be lehet lépni. pl. V8 uszoda / fitness).
##### photo_url 
A felhasználó fényképe ezt ellenőrzés miatt mindenképp kötelező 
megjeleníteni a felületen
#####subscriber_first_name
Felhasználó keresztneve
##### subscriber_last_name
Felhasználó vezetékneve
##### subscriber_hash
Felhasználó egyedi azonosítója (rendszer tag külső kulcsa) -->